import com.autum.board.BoardApplication;
import config.TestConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest(classes = BoardApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = TestConfig.class)
public class SmokeTest extends AbstractTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate client;

    @Test
    public void getPublication_notFound() {
        var uuid = "BNV34CHFD32423YREOI43897FDFDFE78";
        var url = TRANSFER_PROTOCOL.toLowerCase() + "://localhost:" + port + "/api/v1/publications/" + uuid;
        var result = client.getForEntity(url, String.class);
        assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
    }
}