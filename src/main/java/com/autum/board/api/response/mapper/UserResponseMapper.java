package com.autum.board.api.response.mapper;

import com.autum.board.api.response.PublicationResponse;
import com.autum.board.business.publication.dto.PublicationDto;
import com.autum.board.infrastructure.mapstruct.MainMapper;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserResponseMapper extends MainMapper<PublicationDto, PublicationResponse> {

    @Override
    PublicationResponse map(PublicationDto dto);
}