package com.autum.board.api.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.util.List;

@Getter
@Setter
public class PublicationResponse {

    @Schema(description = "Public publication ID")
    private String uuid;
    @Schema(description = "Public user ID")
    private String userUuid;
    @Schema(description = "Publication title")
    private String title;
    @Schema(description = "Publication text")
    private String description;
    @Schema(description = "Publication creation date")
    private Long createdAt;
    @Schema(description = "Publication status")
    private String status;
    @Schema(description = "Image links")
    private List<String> imageUrls;

}