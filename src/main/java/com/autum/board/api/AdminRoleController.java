package com.autum.board.api;

import com.autum.board.business.publication.PublicationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import static com.autum.board.api.util.JwtUtil.ADMIN;
import static org.springframework.http.HttpStatus.OK;


@Tag(name = "ADMIN ROLE API")
@Secured(ADMIN)
@RestController
@AllArgsConstructor
@RequestMapping(value = "/api/v1/admin/publications")
public class AdminRoleController {

    private final PublicationService publicationService;


    @Operation(summary = "Deleting a publication")
    @DeleteMapping("/{uuid}")
    @ResponseStatus(code = OK)
    public void delete(@PathVariable String uuid) {
        publicationService.delete(uuid);
    }
}