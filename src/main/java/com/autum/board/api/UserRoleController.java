package com.autum.board.api;

import com.autum.board.api.response.PublicationResponse;
import com.autum.board.business.publication.PublicationService;
import com.autum.board.business.publication.dto.CreatePublicationDto;
import com.autum.board.infrastructure.mapstruct.Mapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

import static com.autum.board.api.util.JwtUtil.*;
import static org.springframework.http.HttpStatus.OK;


/**
 * /swagger-ui/index.html
 */
@Tag(name = "VIEWER ROLE API")
@Secured(USER)
@RestController
@AllArgsConstructor
@RequestMapping(value = "/api/v1/publications")
public class UserRoleController {

    private final PublicationService publicationService;
    private final Mapper mapper;


    @Operation(summary = "Creating a publication")
    @PostMapping("/create")
    @ResponseStatus(code = OK)
    public PublicationResponse create(String title, String description, MultipartFile[] images, @AuthenticationPrincipal Jwt jwt) {
        var dto = publicationService.create(CreatePublicationDto.builder()
                .title(title)
                .description(description)
                .images(List.of(images))
                .build(), jwt.getTokenValue()
        );
        return mapper.map(dto, PublicationResponse.class);
    }

    @Operation(summary = "Deleting a publication")
    @DeleteMapping("/{uuid}")
    @ResponseStatus(code = OK)
    public void delete(@PathVariable String uuid, @AuthenticationPrincipal Jwt jwt) {
        publicationService.delete(uuid, jwt.getTokenValue());
    }
}