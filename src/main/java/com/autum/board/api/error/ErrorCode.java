package com.autum.board.api.error;

import com.autum.board.business.publication.exception.PublicationNotFoundException;

public enum ErrorCode {

    PUBLICATION_NOT_FOUND,
    INVALID_MEDIA_TYPE,
    INTERNAL_ERROR;

    public static ErrorCode getErrorCode(Throwable e) {
        if (e instanceof PublicationNotFoundException) {
            return PUBLICATION_NOT_FOUND;
        } else {
            return INTERNAL_ERROR;
        }
    }
}