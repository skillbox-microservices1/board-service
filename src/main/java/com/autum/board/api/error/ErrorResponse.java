package com.autum.board.api.error;

import lombok.*;

import java.util.Map;

@AllArgsConstructor
@Getter
public class ErrorResponse {

    private ErrorCode errorCode;
    private String message;
    private Map<String, String> details;

}