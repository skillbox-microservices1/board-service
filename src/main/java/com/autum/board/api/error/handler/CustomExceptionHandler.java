package com.autum.board.api.error.handler;

import com.autum.board.api.error.ErrorResponse;
import com.autum.board.business.provider.ProviderException;
import com.autum.board.business.publication.exception.PublicationNotFoundException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;


import java.util.Locale;

import static com.autum.board.api.error.ErrorCode.*;

@Slf4j
@ControllerAdvice
@AllArgsConstructor
public class CustomExceptionHandler {

    private final MessageSource messageSource;

    @ExceptionHandler(PublicationNotFoundException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorResponse publicationNotFound(PublicationNotFoundException e, Locale locale) {
        log.trace("Publication not found exception");
        var code = getErrorCode(e);
        var msg = messageSource.getMessage(code.name(), null, locale);
        return new ErrorResponse(code, msg, null);
    }

    @ExceptionHandler(Throwable.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse internalError(Throwable e, Locale locale) {
        log.error("Unexpected error", e);
        var msg = messageSource.getMessage(INTERNAL_ERROR.name(), null, locale);
        return new ErrorResponse(INTERNAL_ERROR, msg, null);
    }

    @ResponseBody
    @ExceptionHandler(HttpMediaTypeNotAcceptableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleHttpMediaTypeNotAcceptableException(HttpMediaTypeNotAcceptableException e, Locale locale) {
        log.trace("Invalid media type");
        var msg = messageSource.getMessage(INVALID_MEDIA_TYPE.name(), null, locale);
        return new ErrorResponse(INVALID_MEDIA_TYPE, msg, null);
    }

    @ExceptionHandler(ProviderException.class)
    public ResponseEntity<String> providerException(ProviderException e) {
        log.trace("Provider exception", e);
        return ResponseEntity.status(e.getCode())
                .body(e.getBody());
    }
}
