package com.autum.board.api.util;


public abstract class JwtUtil {

    public static final String USER = "BOARD_VIEWER";
    public static final String ADMIN = "BOARD_ADMIN";

    private JwtUtil() {
    }

}