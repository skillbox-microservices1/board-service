package com.autum.board.api;

import com.autum.board.api.response.PublicationResponse;
import com.autum.board.business.publication.PublicationService;
import com.autum.board.infrastructure.mapstruct.Mapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Tag(name = "SHARED API")
@RestController
@AllArgsConstructor
@RequestMapping(value = "/api/v1/publications")
public class SharedController {

    private PublicationService publicationService;
    private Mapper mapper;


    @Operation(summary = "Getting a publication")
    @GetMapping(value = "/{uuid}")
    public PublicationResponse get(@PathVariable String uuid) {
        var dto = publicationService.get(uuid);
        return mapper.map(dto, PublicationResponse.class);
    }
}