package com.autum.board.utils;

import java.util.UUID;

import static com.autum.board.utils.DateUtil.getTimeNowMillisUTC;

public class Generator {

    public static String generateUUID() {
        var time = String.valueOf(getTimeNowMillisUTC());
        return UUID.nameUUIDFromBytes(time.getBytes())
                .toString()
                .replaceAll("-", "")
                .toUpperCase();
    }
}