package com.autum.board.properties;


import com.amazonaws.Protocol;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "aws-storage")
public class FileStorageProperties {

    //TODO валидации url, на конце - /
    private String endpoint;
    private String externalHost;
    private String singer;
    private String accessKey;
    private String secretKey;
    private String region;
    private String bucket;
    private Protocol protocol = Protocol.HTTP;

}