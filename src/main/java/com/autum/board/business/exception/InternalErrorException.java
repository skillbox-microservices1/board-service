package com.autum.board.business.exception;

public class InternalErrorException extends RuntimeException {

    public InternalErrorException(Throwable e) {
        super(e);
    }
}