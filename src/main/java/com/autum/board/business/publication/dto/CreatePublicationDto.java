package com.autum.board.business.publication.dto;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Getter
@Builder
public class CreatePublicationDto {

    private String title;
    private String description;
    private List<MultipartFile> images;
}