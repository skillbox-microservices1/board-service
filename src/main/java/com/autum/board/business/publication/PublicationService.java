package com.autum.board.business.publication;

import com.autum.board.business.publication.dto.CreatePublicationDto;
import com.autum.board.business.publication.dto.PublicationDto;

public interface PublicationService {

    PublicationDto get(String uuid);

    PublicationDto create(CreatePublicationDto dto, String accessToken);

    void delete(String uuid, String accessToken);

    void delete(String uuid);
}