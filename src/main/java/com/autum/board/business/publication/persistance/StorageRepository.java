package com.autum.board.business.publication.persistance;


import java.io.InputStream;
import java.util.List;


public interface StorageRepository {

    List<String> fileNameList(String prefix);

    void put(String path, InputStream stream, Long size);

    void delete(String prefix);
}