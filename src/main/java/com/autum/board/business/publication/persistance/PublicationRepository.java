package com.autum.board.business.publication.persistance;

import com.autum.board.business.publication.dto.PublicationDto;


public interface PublicationRepository {

    PublicationDto get(String uuid);

    PublicationDto getByUuidAndUserUuid(String uuid, String userUuid);

    PublicationDto save(PublicationDto dto);

}