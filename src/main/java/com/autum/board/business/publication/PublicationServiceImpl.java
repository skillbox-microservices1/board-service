package com.autum.board.business.publication;

import com.autum.board.business.exception.InternalErrorException;
import com.autum.board.business.provider.user.UserProvider;
import com.autum.board.business.publication.dto.CreatePublicationDto;
import com.autum.board.business.publication.dto.PublicationDto;
import com.autum.board.business.publication.exception.PublicationNotFoundException;
import com.autum.board.business.publication.persistance.PublicationRepository;
import com.autum.board.business.publication.persistance.StorageRepository;
import com.autum.board.properties.FileStorageProperties;
import com.autum.board.utils.DateUtil;
import com.autum.board.utils.Generator;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


import javax.transaction.Transactional;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static com.autum.board.business.publication.dto.PublicationDto.Status.ACTIVE;
import static com.autum.board.business.publication.dto.PublicationDto.Status.DELETED;


@Service
@Slf4j
@AllArgsConstructor
public class PublicationServiceImpl implements PublicationService {

    private final PublicationRepository pubRepo;
    private final StorageRepository storage;
    private final FileStorageProperties fileStorageProperties;
    private final UserProvider userProvider;

    private static final String EXTENSION = ".jpeg";

    @Override
    public PublicationDto get(String uuid) {
        log.trace("Get publication, uuid = {}", uuid);
        var publicationDto = pubRepo.get(uuid);
        checkPublication(publicationDto);
        var prefix = getPrefix(publicationDto);
        var urls = storage.fileNameList(prefix);
        publicationDto.setImageUrls(buildUrls(urls));
        return publicationDto;
    }

    private List<String> buildUrls(List<String> fileName) {
        return fileName.stream()
                .map(res -> fileStorageProperties.getExternalHost() + fileStorageProperties.getBucket() + "/" + res)
                .collect(Collectors.toList());
    }

    //TODO к названию файлов прикрутить счетчик
    @Transactional
    @Override
    public PublicationDto create(CreatePublicationDto dto, String accessToken) {
        log.trace("Create publication, title = {}, files = {} ", dto.getTitle(), dto.getImages().size());
        var user = userProvider.getUser(accessToken);
        var pub = createPublication(dto, user.getUuid());
        var savedDto = pubRepo.save(pub);
        for (MultipartFile mf : dto.getImages()) {
            var fileName = Generator.generateUUID();
            try {
                var path = getPath(savedDto, fileName);
                storage.put(path, mf.getInputStream(), mf.getSize());
            } catch (IOException e) {
                throw new InternalErrorException(e);
            }
        }
        return savedDto;
    }

    private void checkPublication(PublicationDto dto) {
        if (dto == null) {
            throw new PublicationNotFoundException();
        }
    }

    private PublicationDto createPublication(CreatePublicationDto dto, String userUuid) {
        var time = DateUtil.getTimeNowMillisUTC();
        return PublicationDto.builder()
                .uuid(Generator.generateUUID())
                .userUuid(userUuid)
                .title(dto.getTitle())
                .description(dto.getDescription())
                .createdAt(time)
                .status(ACTIVE)
                .build();
    }

    @Override
    @Transactional
    public void delete(String uuid, String accessToken) {
        log.trace("Deleted publication: uuid = {} by user", uuid);
        var user = userProvider.getUser(accessToken);
        var publicationDto = pubRepo.getByUuidAndUserUuid(uuid, user.getUuid());
        checkPublication(publicationDto);
        publicationDto.setDeletedAt(DateUtil.getTimeNowMillisUTC());
        publicationDto.setStatus(DELETED);
        pubRepo.save(publicationDto);
        storage.delete(getPrefix(publicationDto));
    }

    @Override
    public void delete(String uuid) {
        log.trace("Deleted publication: uuid = {}", uuid);
        var publicationDto = pubRepo.get(uuid);
        checkPublication(publicationDto);
        publicationDto.setDeletedAt(DateUtil.getTimeNowMillisUTC());
        publicationDto.setStatus(DELETED);
        pubRepo.save(publicationDto);
        storage.delete(getPrefix(publicationDto));
    }

    private String getPath(PublicationDto dto, String fileName) {
        return dto.getUserUuid() + "/" + dto.getUuid() + "/" + fileName + EXTENSION;
    }

    private String getPrefix(PublicationDto dto) {
        return dto.getUserUuid() + "/" + dto.getUuid() + "/";
    }
}