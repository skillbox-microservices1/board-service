package com.autum.board.business.publication.dto;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
@EqualsAndHashCode
public class PublicationDto {

    private Long id;
    private String uuid;
    private String userUuid;
    private String title;
    private String description;
    private Long createdAt;
    private Long deletedAt;
    private Status status;
    private List<String> imageUrls;

    public enum Status {
        ACTIVE,
        IN_ARCHIVE,
        DELETED;

        public static Status fromInteger(int status) {
            switch (status) {
                case 1:
                    return ACTIVE;
                case 2:
                    return IN_ARCHIVE;
                case 3:
                    return DELETED;
                default:
                    throw new IllegalArgumentException("Integer " + status + "not processed!");
            }
        }

        public static int toInteger(Status status) {
            switch (status) {
                case ACTIVE:
                    return 1;
                case IN_ARCHIVE:
                    return 2;
                case DELETED:
                    return 3;
                default:
                    throw new IllegalArgumentException("Status " + status + "not processed!");
            }
        }

    }
}