package com.autum.board.business.provider.user;

import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
public class UserDto {

    private String uuid;
    private String firstName;
    private String lastName;
    private String middleName;
    private CityDto city;
    private List<SkillDto> skills;
    private String avatarUrl;
    private String about;
    private Status status;
    private Sex sex;
    private Long subscriptions;
    private Long subscribers;

    public enum Status {
        ACTIVE,
        BLOCKED
    }

    public enum Sex {
        MALE,
        FEMALE,
        UNKNOWN;
    }

    @Getter
    @Setter
    public static class CityDto {
        private String uuid;
        private String name;
        private String region;
        private String country;
    }

    @Getter
    @Setter
    public static class SkillDto {
        private String uuid;
        private String name;
        private Integer level;
    }
}