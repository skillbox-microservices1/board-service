package com.autum.board.business.provider.user;

import com.autum.board.business.provider.ProviderException;


public interface UserProvider {

    UserDto getUser(String accessToken) throws ProviderException;
}