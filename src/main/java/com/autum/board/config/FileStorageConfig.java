package com.autum.board.config;

import com.autum.board.properties.FileStorageProperties;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class FileStorageConfig {

    @Bean
    public AmazonS3 storage(FileStorageProperties properties) {
        var basicCredentials = new BasicAWSCredentials(properties.getAccessKey(), properties.getSecretKey());
        var credentials = new AWSStaticCredentialsProvider(basicCredentials);
        var endpoint = new AwsClientBuilder.EndpointConfiguration(properties.getEndpoint(), properties.getRegion());
        var clientConfiguration = new ClientConfiguration();
        clientConfiguration.withProtocol(properties.getProtocol());
        clientConfiguration.setSignerOverride(properties.getSinger());
        return AmazonS3ClientBuilder
                .standard()
                .withEndpointConfiguration(endpoint)
                .withPathStyleAccessEnabled(true)
                .withClientConfiguration(clientConfiguration)
                .withCredentials(credentials)
                .build();
    }
}