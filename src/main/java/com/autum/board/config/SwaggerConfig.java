package com.autum.board.config;

import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class SwaggerConfig {

    @Bean
    public GroupedOpenApi api() {
        return GroupedOpenApi.builder()
                .group("BOARD API")
                .addOpenApiCustomiser(openApi ->
                        openApi
                                .addSecurityItem(new SecurityRequirement().addList("Authorization"))
                                .getComponents()
                                .addSecuritySchemes("Authorization", new SecurityScheme()
                                        .in(SecurityScheme.In.HEADER)
                                        .type(SecurityScheme.Type.HTTP)
                                        .scheme("bearer")
                                        .name("JWT")
                                ))
                .pathsToMatch("/api/**")
                .build();
    }
}