package com.autum.board.provider;


import com.autum.board.business.provider.ProviderException;
import com.autum.board.business.provider.user.UserDto;
import com.autum.board.business.provider.user.UserProvider;
import com.autum.board.properties.UserProperties;
import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;


@Component
@AllArgsConstructor
public class UserProviderImpl implements UserProvider {

    private final RestTemplate restTemplate;
    private final UserProperties userProperties;
    private final Gson gson;


    @Override
    public UserDto getUser(String accessToken) throws ProviderException {
        var url = userProperties.getUrl() + "/api/v1/users/yourself";

        var headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + accessToken);

        var response = restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), String.class);
        if (response.getStatusCode() == HttpStatus.OK) {
            return gson.fromJson(response.getBody(), UserDto.class);
        } else {
            throw new ProviderException(response.getStatusCode().value(), response.getBody());
        }
    }
}