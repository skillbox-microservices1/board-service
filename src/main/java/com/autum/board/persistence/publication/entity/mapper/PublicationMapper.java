package com.autum.board.persistence.publication.entity.mapper;

import com.autum.board.business.publication.dto.PublicationDto;
import com.autum.board.infrastructure.mapstruct.MainMapper;
import com.autum.board.infrastructure.mapstruct.converter.EnumConverter;
import com.autum.board.infrastructure.mapstruct.converter.TimeConverter;
import com.autum.board.persistence.publication.entity.Publication;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface PublicationMapper extends MainMapper<PublicationDto, Publication>, TimeConverter, EnumConverter {

    @Mappings({
            @Mapping(target = "createdAt", source = "createdAt", qualifiedByName = "LongToLocalDateTime"),
            @Mapping(target = "deletedAt", source = "deletedAt", qualifiedByName = "LongToLocalDateTime"),
            @Mapping(target = "status", source = "status", qualifiedByName = "EnumStatusToInteger"),
    })
    @Override
    Publication map(PublicationDto dto);
}