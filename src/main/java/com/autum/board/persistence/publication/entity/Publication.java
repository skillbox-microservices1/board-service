package com.autum.board.persistence.publication.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "publications")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Publication {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private String uuid;
    private String userUuid;
    private String title;
    private String description;
    private LocalDateTime createdAt;
    private LocalDateTime deletedAt;
    private Integer status;
}