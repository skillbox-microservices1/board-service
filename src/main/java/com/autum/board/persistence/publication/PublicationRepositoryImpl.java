package com.autum.board.persistence.publication;

import com.autum.board.business.publication.dto.PublicationDto;
import com.autum.board.business.publication.persistance.PublicationRepository;
import com.autum.board.infrastructure.mapstruct.Mapper;
import com.autum.board.persistence.publication.entity.Publication;
import com.autum.board.persistence.publication.repodb.PublicationRepositoryDb;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;


@Component
@AllArgsConstructor
public class PublicationRepositoryImpl implements PublicationRepository {

    private final PublicationRepositoryDb repositoryDb;
    private final Mapper mapper;


    @Override
    public PublicationDto get(String uuid) {
        var publication = repositoryDb.findByUuid(uuid);
        return mapper.map(publication, PublicationDto.class);
    }

    @Override
    public PublicationDto getByUuidAndUserUuid(String uuid, String userUuid) {
        var publication = repositoryDb.findByUuidAndUserUuid(uuid, userUuid);
        return mapper.map(publication, PublicationDto.class);
    }

    @Override
    public PublicationDto save(PublicationDto dto) {
        var publication = mapper.map(dto, Publication.class);
        var savedPublication = repositoryDb.save(publication);
        return mapper.map(savedPublication, PublicationDto.class);
    }
}