package com.autum.board.persistence.publication.repodb;

import com.autum.board.persistence.publication.entity.Publication;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface PublicationRepositoryDb extends CrudRepository<Publication, Long> {

    @Query(value = "SELECT * FROM publications WHERE uuid = :uuid AND status <> 3", nativeQuery = true)
    Publication findByUuid(String uuid);

    @Query(value = "SELECT * FROM publications WHERE uuid = :uuid AND " +
            "user_uuid =:userUuid AND status <> 3", nativeQuery = true)
    Publication findByUuidAndUserUuid(String uuid, String userUuid);

}