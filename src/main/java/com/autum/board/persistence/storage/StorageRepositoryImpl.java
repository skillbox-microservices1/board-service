package com.autum.board.persistence.storage;

import com.autum.board.business.publication.persistance.StorageRepository;
import com.autum.board.properties.FileStorageProperties;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;


import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
@AllArgsConstructor
public class StorageRepositoryImpl implements StorageRepository {

    private final AmazonS3 storage;
    private final FileStorageProperties properties;

    public List<String> fileNameList(String prefix) {
        log.trace("Get file name list: prefix={}", prefix);
        return storage.listObjectsV2(properties.getBucket(), prefix)
                .getObjectSummaries().stream()
                .map(S3ObjectSummary::getKey)
                .collect(Collectors.toList());
    }

    public void put(String path, InputStream stream, Long size) {
        log.trace("Put file: path={}", path);
        var metadata = new ObjectMetadata();
        metadata.setContentLength(size);
        storage.putObject(properties.getBucket(), path, stream, metadata);
    }

    //TODO если файла нет ошибка?
    public void delete(String prefix) {
        log.trace("Delete: prefix={}", prefix);
        var bucket = properties.getBucket();
        var keys = getDeleteObjectsRequestList(prefix, bucket);
        //TODO возвращается путь или только имя файла?
        if (!keys.isEmpty()) { //TODO ?
            var req = new DeleteObjectsRequest(bucket);
            req.setKeys(keys);
            storage.deleteObjects(req);
        }
    }

    private List<DeleteObjectsRequest.KeyVersion> getDeleteObjectsRequestList(String prefix, String bucket) {
        return storage.listObjectsV2(bucket, prefix)
                .getObjectSummaries().stream()
                .map(S3ObjectSummary::getKey)
                .peek(key -> log.trace("Key for delete: {}", key))
                .map(DeleteObjectsRequest.KeyVersion::new)
                .collect(Collectors.toList());
    }
}