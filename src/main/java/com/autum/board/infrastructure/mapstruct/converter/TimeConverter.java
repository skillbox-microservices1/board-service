package com.autum.board.infrastructure.mapstruct.converter;


import com.autum.board.utils.DateUtil;
import org.mapstruct.Named;

import java.time.LocalDateTime;

public interface TimeConverter {

    @Named("LocalDateTimeToLong")
    default Long localDateTimeToLong(LocalDateTime time) {
        return time == null ? null : DateUtil.getMillisFromLocalDateTime(time);
    }

    @Named("LongToLocalDateTime")
    default LocalDateTime longToLocalDateTime(Long time) {
        return time == null ? null : DateUtil.getLocalDateTimeUTC(time);
    }
}