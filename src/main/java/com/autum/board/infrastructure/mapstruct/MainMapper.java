package com.autum.board.infrastructure.mapstruct;

public interface MainMapper<T, E> {
    E map(T t);
}