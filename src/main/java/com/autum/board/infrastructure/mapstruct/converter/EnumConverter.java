package com.autum.board.infrastructure.mapstruct.converter;


import com.autum.board.business.publication.dto.PublicationDto;
import org.mapstruct.Named;

public interface EnumConverter {

    @Named("IntegerToEnumStatus")
    default PublicationDto.Status integerToEnumStatus(Integer status) {
        return status == null ? null : PublicationDto.Status.fromInteger(status);
    }

    @Named("EnumStatusToInteger")
    default Integer enumStatusToInteger(PublicationDto.Status status) {
        return status == null ? null : PublicationDto.Status.toInteger(status);
    }
}