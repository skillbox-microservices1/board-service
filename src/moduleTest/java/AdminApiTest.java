import com.amazonaws.services.s3.AmazonS3;
import com.autum.board.BoardApplication;
import com.autum.board.properties.FileStorageProperties;
import config.AdminApiPostgresConfig;
import config.TestConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.PostConstruct;

import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest(classes = BoardApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = {AdminApiPostgresConfig.class, TestConfig.class})
public class AdminApiTest extends AbstractTest {

    @LocalServerPort
    private int port;

    private String basUrl;
    private HttpHeaders headersWithToken;

    @Autowired
    private TestRestTemplate restTemplate;
    @Autowired
    private AmazonS3 storage;
    @Autowired
    private FileStorageProperties storageProperties;


    @PostConstruct
    public void init() {
        if (!storage.doesBucketExistV2(storageProperties.getBucket())) {
            storage.createBucket(storageProperties.getBucket());
        }
        headersWithToken = getHeadersWithAccessTokenAndJson();
        basUrl = AbstractTest.TRANSFER_PROTOCOL + "://localhost:" + port + "/api/v1/admin/publications";
    }

    @Test
    public void delete() {
        var publicationUUid = "R3345EER34IJREI23423JERI234EF5VB";
        var url = basUrl + "/" + publicationUUid;
        var httpEntity = new HttpEntity<>(null, headersWithToken);
        var response = restTemplate.exchange(url, HttpMethod.DELETE, httpEntity, Void.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        //TODO проверка базы
    }

    private HttpHeaders getHeadersWithAccessTokenAndJson() {
        var accessToken = getAccessToken("admin", restTemplate);
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + accessToken);
        return headers;
    }
}
