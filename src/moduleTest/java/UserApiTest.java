import com.amazonaws.services.s3.AmazonS3;
import com.autum.board.BoardApplication;
import com.autum.board.business.provider.user.UserDto;
import com.autum.board.properties.FileStorageProperties;
import com.google.gson.Gson;
import config.TestConfig;
import config.UserApiPostgresConfig;
import org.junit.jupiter.api.Test;
import org.mockserver.client.MockServerClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.util.LinkedMultiValueMap;

import javax.annotation.PostConstruct;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;


@SpringBootTest(classes = BoardApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = {UserApiPostgresConfig.class, TestConfig.class})
public class UserApiTest extends AbstractTest {

    @LocalServerPort
    private int port;
    private String baseUrl;

    private final MockServerClient mockServerClient = new MockServerClient(MOCK_HTTP_SERVER.getHost(), MOCK_HTTP_SERVER.getServerPort());

    @Autowired
    private TestRestTemplate client;
    @Autowired
    private AmazonS3 storage;
    @Autowired
    private FileStorageProperties storageProperties;


    @PostConstruct
    public void init() {
        if (!storage.doesBucketExistV2(storageProperties.getBucket())) {
            storage.createBucket(storageProperties.getBucket());
        }
        baseUrl = TRANSFER_PROTOCOL.toLowerCase() + "://localhost:" + port + "/api/v1/publications";
    }

    @Test
    public void createPublication() throws Exception {
        var username = "autum";
        var image = new ClassPathResource("java.jpg");

        var accessToken = getAccessToken(username, client);
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        var auth = "Bearer " + accessToken;
        headers.set("Authorization", "Bearer " + accessToken);

        LinkedMultiValueMap<String, Object> multipartRequestParts = new LinkedMultiValueMap<>();
        multipartRequestParts.add("title", "Main post");
        multipartRequestParts.add("description", "first post");
        multipartRequestParts.add("images", new FileSystemResource(image.getFile()));

        var userDto = new UserDto();
        userDto.setUuid("RUFDCV7890FDERE6709DEWDE324VBFDF");
        userDto.setFirstName("Nick");
        userDto.setLastName("One");
        userDto.setMiddleName("Wan");

        var city = new UserDto.CityDto();
        city.setUuid("11FDCV7890FDERE6709DEWDE324VB321");
        city.setName("Asgard");
        city.setCountry("Asgard");
        city.setRegion("Valhalla");

        userDto.setCity(city);
        userDto.setStatus(UserDto.Status.ACTIVE);
        userDto.setSex(UserDto.Sex.MALE);
        userDto.setSubscriptions(100L);
        userDto.setSubscribers(100L);

        mockServerClient
                .when(request()
                        .withMethod("GET")
                        .withPath("/api/v1/users/yourself")
                        .withHeader("Authorization", auth)
                )
                .respond(response()
                        .withBody(new Gson().toJson(userDto))
                );

        var response = client.exchange(
                "/api/v1/publications/create",
                HttpMethod.POST,
                new HttpEntity<>(multipartRequestParts, headers),
                String.class
        );

        assertEquals(HttpStatus.OK, response.getStatusCode());

        //TODO проверка базы
    }

    @Test
    public void delete() {
        var publicationUUid = "R3345EER34IJREI23423JERI234EF5VB";
        var url = baseUrl + "/" + publicationUUid;
        var username = "autum";
        var accessToken = getAccessToken(username, client);
        var headers = new HttpHeaders();
        var auth = "Bearer " + accessToken;
        headers.set("Authorization", "Bearer " + accessToken);

        var userDto = new UserDto();
        userDto.setUuid("11345EER34IJREI23423JERI234EF5VB");
        userDto.setFirstName("Nick");
        userDto.setLastName("One");
        userDto.setMiddleName("Wan");

        var city = new UserDto.CityDto();
        city.setUuid("11FDCV7890FDERE6709DEWDE324VB321");
        city.setName("Asgard");
        city.setCountry("Asgard");
        city.setRegion("Valhalla");

        userDto.setCity(city);
        userDto.setStatus(UserDto.Status.ACTIVE);
        userDto.setSex(UserDto.Sex.MALE);
        userDto.setSubscriptions(100L);
        userDto.setSubscribers(100L);

        mockServerClient
                .when(request()
                        .withMethod("GET")
                        .withPath("/api/v1/users/yourself")
                        .withHeader("Authorization", auth)
                )
                .respond(response()
                        .withBody(new Gson().toJson(userDto))
                );

        var httpEntity = new HttpEntity<>(null, headers);
        var response = client.exchange(url, HttpMethod.DELETE, httpEntity, Void.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        //TODO проверка базы
    }
}
