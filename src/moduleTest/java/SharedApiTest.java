import com.amazonaws.services.s3.AmazonS3;
import com.autum.board.BoardApplication;
import com.autum.board.api.response.PublicationResponse;
import com.autum.board.properties.FileStorageProperties;
import config.SharedApiPostgresConfig;
import config.TestConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.PostConstruct;

import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest(classes = BoardApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = {SharedApiPostgresConfig.class, TestConfig.class})
public class SharedApiTest extends AbstractTest {

    @LocalServerPort
    private int port;

    private String baseUrl;

    @Autowired
    private TestRestTemplate client;
    @Autowired
    private AmazonS3 storage;
    @Autowired
    private FileStorageProperties storageProperties;


    @PostConstruct
    public void init() {
        if (!storage.doesBucketExistV2(storageProperties.getBucket())) {
            storage.createBucket(storageProperties.getBucket());
        }
        baseUrl = AbstractTest.TRANSFER_PROTOCOL.toLowerCase() + "://localhost:" + port + "/api/v1/publications";
    }

    @Test
    public void get() {
        var publicationUUid = "R3345EER34IJREI23423JERI234EF5VB";
        var url = baseUrl + "/" + publicationUUid;
        var response = client.exchange(url, HttpMethod.GET, null, PublicationResponse.class);
        //TODO проверка body
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }
}
