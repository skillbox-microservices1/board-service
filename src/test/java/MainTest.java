import com.autum.board.BoardApplication;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.boot.SpringApplication;
import org.springframework.context.support.StaticApplicationContext;


class MainTest {

    @Test
    void mainTest() {
        try (MockedStatic<SpringApplication> spring = Mockito.mockStatic(SpringApplication.class)) {
            var cxt = new StaticApplicationContext();
            var arg = new String[]{"Test"};
            spring.when(() -> SpringApplication.run(BoardApplication.class, arg)).thenReturn(cxt);
            BoardApplication.main(arg);
            spring.verify(() -> SpringApplication.run(BoardApplication.class, arg));
        }
    }
}