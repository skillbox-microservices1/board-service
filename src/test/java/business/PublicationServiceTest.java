package business;

import com.autum.board.business.provider.user.UserDto;
import com.autum.board.business.provider.user.UserProvider;
import com.autum.board.business.publication.PublicationServiceImpl;
import com.autum.board.business.publication.dto.CreatePublicationDto;
import com.autum.board.business.publication.dto.PublicationDto;
import com.autum.board.business.publication.exception.PublicationNotFoundException;
import com.autum.board.persistence.publication.PublicationRepositoryImpl;
import com.autum.board.persistence.storage.StorageRepositoryImpl;
import com.autum.board.properties.FileStorageProperties;
import com.autum.board.utils.DateUtil;
import com.autum.board.utils.Generator;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static com.autum.board.business.publication.dto.PublicationDto.Status.ACTIVE;
import static com.autum.board.business.publication.dto.PublicationDto.Status.DELETED;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;


class PublicationServiceTest {

    @Test
    void testGet() {

        var uuid = "UIOPFGDERTYDSAZXC78965GH67UIO896";
        var userUuid = "USERUUID12FGT33413VBCGGDYREI7878";
        var bucket = "test-bucket";
        var prefix = userUuid + "/" + uuid + "/";
        var list = List.of(prefix + "file1.jpeg", prefix + "file2.jpeg");
        var host = "https://example.com/";
        var createdAt = System.currentTimeMillis();

        var pubRepo = mock(PublicationRepositoryImpl.class);
        var storage = mock(StorageRepositoryImpl.class);
        var properties = mock(FileStorageProperties.class);
        var service = new PublicationServiceImpl(pubRepo, storage, properties, null);

        var publicationDto = PublicationDto.builder()
                .id(1L)
                .uuid(uuid)
                .userUuid(userUuid)
                .title("Test title")
                .description("Test description")
                .createdAt(createdAt)
                .status(PublicationDto.Status.ACTIVE)
                .build();

        when(pubRepo.get(uuid)).thenReturn(publicationDto);
        when(storage.fileNameList(prefix)).thenReturn(list);
        when(properties.getExternalHost()).thenReturn(host);
        when(properties.getBucket()).thenReturn(bucket);

        var result = service.get(uuid);

        assertEquals(publicationDto.getId(), result.getId());
        assertEquals(publicationDto.getUuid(), result.getUuid());
        assertEquals(publicationDto.getUserUuid(), result.getUserUuid());
        assertEquals(publicationDto.getTitle(), result.getTitle());
        assertEquals(publicationDto.getDescription(), result.getDescription());
        assertEquals(publicationDto.getCreatedAt(), result.getCreatedAt());
        assertEquals(publicationDto.getStatus(), result.getStatus());
        assertTrue(result.getImageUrls().contains(host + bucket + "/" + prefix + "file1.jpeg"));
        assertTrue(result.getImageUrls().contains(host + bucket + "/" + prefix + "file2.jpeg"));

        verify(pubRepo, times(1)).get(uuid);
        verify(storage, times(1)).fileNameList(prefix);
        verify(properties, times(2)).getExternalHost();
        verify(properties, times(2)).getBucket();
    }

    @Test
    void testGet_null() {
        var uuid = "UIOPFGDERTYDSAZXC78965GH67UIO896";

        var pubRepo = mock(PublicationRepositoryImpl.class);
        var service = new PublicationServiceImpl(pubRepo, null, null, null);

        when(pubRepo.get(uuid)).thenReturn(null);

        assertThrows(PublicationNotFoundException.class, () -> service.get(uuid));

        verify(pubRepo, times(1)).get(uuid);
    }

    @Test
    void testCreate() throws IOException {
        var time = DateUtil.getTimeNowMillisUTC();
        var uuid = Generator.generateUUID();

        var accessToken = "ffwefresdvf23423oijjfoisdjfoidwjeoijkoi32432jioFOEJFOIESJoi3423";
        var userUuid = "USERUUID12FGT33413VBCGGDYREI7878";

        var pubRepo = mock(PublicationRepositoryImpl.class);
        var storage = mock(StorageRepositoryImpl.class);
        var properties = mock(FileStorageProperties.class);
        var userProvider = mock(UserProvider.class);
        var service = new PublicationServiceImpl(pubRepo, storage, properties, userProvider);

        MultipartFile image1 = new MockMultipartFile("file", "file1.jpeg", "image/jpeg", new byte[0]);
        MultipartFile image2 = new MockMultipartFile("file", "file2.jpeg", "image/jpeg", new byte[0]);
        List<MultipartFile> images = List.of(image1, image2);

        var createDto = CreatePublicationDto.builder()
                .title("Test title")
                .description("Test description")
                .images(images)
                .build();

        var pubDto = PublicationDto.builder()
                .uuid(uuid)
                .userUuid(userUuid)
                .title(createDto.getTitle())
                .description(createDto.getDescription())
                .createdAt(time)
                .status(ACTIVE)
                .build();

        var savedPubDto = PublicationDto.builder()
                .id(1L)
                .uuid(pubDto.getUuid())
                .userUuid(pubDto.getUserUuid())
                .title(pubDto.getTitle())
                .description(pubDto.getDescription())
                .createdAt(pubDto.getCreatedAt())
                .status(pubDto.getStatus())
                .build();

        var pathImage1 = userUuid + "/" + uuid + "/" + uuid + ".jpeg";
        var pathImage2 = userUuid + "/" + uuid + "/" + uuid + ".jpeg";

        var userDto = new UserDto();
        userDto.setUuid(userUuid);
        userDto.setFirstName("First");
        userDto.setLastName("Last");
        userDto.setStatus(UserDto.Status.ACTIVE);
        userDto.setSex(UserDto.Sex.MALE);
        userDto.setSubscriptions(0L);
        userDto.setSubscribers(0L);

        when(pubRepo.save(pubDto)).thenReturn(savedPubDto);
        when(userProvider.getUser(accessToken)).thenReturn(userDto);
        doNothing().when(storage).put(eq(pathImage1), any(InputStream.class), eq(image1.getSize()));
        doNothing().when(storage).put(eq(pathImage2), any(InputStream.class), eq(image2.getSize()));

        try (MockedStatic<DateUtil> dateUtil = Mockito.mockStatic(DateUtil.class)) {
            dateUtil.when(DateUtil::getTimeNowMillisUTC).thenReturn(time);
            try (MockedStatic<Generator> generator = Mockito.mockStatic(Generator.class)) {
                generator.when(Generator::generateUUID).thenReturn(uuid);

                var result = service.create(createDto, accessToken);
//TODO
            }
        }

        verify(storage, times(2)).put(eq(pathImage1), any(InputStream.class), eq(image1.getSize()));
        verify(storage, times(2)).put(eq(pathImage2), any(InputStream.class), eq(image2.getSize()));
    }

    @Test
    void testDelete() {

        var time = DateUtil.getTimeNowMillisUTC();

        var uuid = "UIOPFGDERTYDSAZXC78965GH67UIO896";
        var accessToken = "ffwefresdvf23423oijjfoisdjfoidwjeoijkoi32432jioFOEJFOIESJoi3423";
        var userUuid = "USERUUID12FGT33413VBCGGDYREI7878";
        var prefix = userUuid + "/" + uuid + "/";

        var pubRepo = mock(PublicationRepositoryImpl.class);
        var storage = mock(StorageRepositoryImpl.class);
        var properties = mock(FileStorageProperties.class);
        var provider = mock(UserProvider.class);
        var service = new PublicationServiceImpl(pubRepo, storage, properties, provider);

        var dto = PublicationDto.builder()
                .id(1L)
                .uuid(uuid)
                .userUuid(userUuid)
                .title("Test title")
                .description("Test description")
                .createdAt(time)
                .status(PublicationDto.Status.ACTIVE)
                .build();

        var deletedDto = PublicationDto.builder()
                .id(dto.getId())
                .uuid(dto.getUuid())
                .userUuid(dto.getUserUuid())
                .title(dto.getTitle())
                .description(dto.getDescription())
                .createdAt(dto.getCreatedAt())
                .deletedAt(time)
                .status(DELETED)
                .build();

        var userDto = new UserDto();
        userDto.setUuid(userUuid);
        userDto.setFirstName("First");
        userDto.setLastName("Last");
        userDto.setStatus(UserDto.Status.ACTIVE);
        userDto.setSex(UserDto.Sex.MALE);
        userDto.setSubscriptions(0L);
        userDto.setSubscribers(0L);

        when(provider.getUser(accessToken)).thenReturn(userDto);
        when(pubRepo.getByUuidAndUserUuid(uuid, userUuid)).thenReturn(dto);
        when(pubRepo.save(deletedDto)).thenReturn(deletedDto);
        doNothing().when(storage).delete(prefix);

        try (MockedStatic<DateUtil> dateUtil = Mockito.mockStatic(DateUtil.class)) {
            dateUtil.when(DateUtil::getTimeNowMillisUTC).thenReturn(time);
            service.delete(uuid, accessToken);
        }

        verify(pubRepo, times(1)).getByUuidAndUserUuid(uuid, userUuid);
        verify(pubRepo, times(1)).save(eq(deletedDto));
        verify(storage, times(1)).delete(prefix);
    }

    @Test
    void testDeleteByUuid() {

        var time = DateUtil.getTimeNowMillisUTC();
        try (MockedStatic<DateUtil> dateUtil = Mockito.mockStatic(DateUtil.class)) {
            dateUtil.when(DateUtil::getTimeNowMillisUTC).thenReturn(time);
        }

        var uuid = "UIOPFGDERTYDSAZXC78965GH67UIO896";
        var userUuid = "USERUUID12FGT33413VBCGGDYREI7878";
        var prefix = userUuid + "/" + uuid + "/";

        var pubRepo = mock(PublicationRepositoryImpl.class);
        var storage = mock(StorageRepositoryImpl.class);
        var properties = mock(FileStorageProperties.class);
        var provider = mock(UserProvider.class);
        var service = new PublicationServiceImpl(pubRepo, storage, properties, provider);

        var dto = PublicationDto.builder()
                .id(1L)
                .uuid(uuid)
                .userUuid(userUuid)
                .title("Test title")
                .description("Test description")
                .createdAt(time)
                .status(PublicationDto.Status.ACTIVE)
                .build();

        var deletedDto = PublicationDto.builder()
                .id(dto.getId())
                .uuid(dto.getUuid())
                .userUuid(dto.getUserUuid())
                .title(dto.getTitle())
                .description(dto.getDescription())
                .createdAt(dto.getCreatedAt())
                .deletedAt(time)
                .status(DELETED)
                .build();

        when(pubRepo.get(uuid)).thenReturn(dto);
        when(pubRepo.save(deletedDto)).thenReturn(deletedDto);
        doNothing().when(storage).delete(prefix);

        try (MockedStatic<DateUtil> dateUtil = Mockito.mockStatic(DateUtil.class)) {
            dateUtil.when(DateUtil::getTimeNowMillisUTC).thenReturn(time);
            service.delete(uuid);
        }

        verify(pubRepo, times(1)).get(uuid);
        verify(pubRepo, times(1)).save(eq(deletedDto));
        verify(storage, times(1)).delete(prefix);
    }
}