package mapstruct;

import com.autum.board.business.publication.dto.PublicationDto;
import com.autum.board.infrastructure.mapstruct.Mapper;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNull;

class MapperTest {

    @Test
    public void null_result() {
        var mapper = new Mapper(null);
        var result = mapper.map(null, PublicationDto.class);
        assertNull(result);
    }
}