package mapstruct;

import com.autum.board.persistence.publication.entity.mapper.PublicationDtoMapperImpl;
import com.autum.board.utils.DateUtil;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static com.autum.board.business.publication.dto.PublicationDto.Status.*;
import static org.junit.jupiter.api.Assertions.*;


class ConverterTest {

    @Test
    void integerToEnumStatus_ACTIVE() {
        var mapper = new PublicationDtoMapperImpl();
        var result = mapper.integerToEnumStatus(1);
        assertEquals(ACTIVE, result);
    }

    @Test
    void integerToEnumStatus_IN_ARCHIVE() {
        var mapper = new PublicationDtoMapperImpl();
        var result = mapper.integerToEnumStatus(2);
        assertEquals(IN_ARCHIVE, result);
    }

    @Test
    void integerToEnumStatus_DELETED() {
        var mapper = new PublicationDtoMapperImpl();
        var result = mapper.integerToEnumStatus(3);
        assertEquals(DELETED, result);
    }

    @Test
    void integerToEnumStatus_null() {
        var mapper = new PublicationDtoMapperImpl();
        var result = mapper.integerToEnumStatus(null);
        assertNull(result);
    }

    @Test
    void integerToEnumStatus_illegalArgument() {
        var mapper = new PublicationDtoMapperImpl();
        assertThrows(IllegalArgumentException.class, () -> mapper.integerToEnumStatus(999));
    }


    @Test
    void enumStatusToInteger_ACTIVE() {
        var mapper = new PublicationDtoMapperImpl();
        var result = mapper.enumStatusToInteger(ACTIVE);
        assertEquals(1, result);
    }

    @Test
    void enumStatusToInteger_IN_ARCHIVE() {
        var mapper = new PublicationDtoMapperImpl();
        var result = mapper.enumStatusToInteger(IN_ARCHIVE);
        assertEquals(2, result);
    }

    @Test
    void enumStatusToInteger_DELETED() {
        var mapper = new PublicationDtoMapperImpl();
        var result = mapper.enumStatusToInteger(DELETED);
        assertEquals(3, result);
    }

    @Test
    void enumStatusToInteger_null() {
        var mapper = new PublicationDtoMapperImpl();
        var result = mapper.enumStatusToInteger(null);
        assertNull(result);
    }

    @Test
    void localDateTimeToLong() {
        var time = LocalDateTime.now();
        var mapper = new PublicationDtoMapperImpl();
        var expected = DateUtil.getMillisFromLocalDateTime(time);
        var result = mapper.localDateTimeToLong(time);
        assertEquals(expected, result);
    }

    @Test
    void localDateTimeToLong_null() {
        var mapper = new PublicationDtoMapperImpl();
        var result = mapper.localDateTimeToLong(null);
        assertNull(result);
    }

    @Test
    void longToLocalDateTime() {
        var time = DateUtil.getTimeNowMillis();
        var mapper = new PublicationDtoMapperImpl();
        var expected = DateUtil.getLocalDateTimeUTC(time);
        var result = mapper.longToLocalDateTime(time);
        assertEquals(expected, result);
    }

    @Test
    void longToLocalDateTime_null() {
        var mapper = new PublicationDtoMapperImpl();
        var result = mapper.longToLocalDateTime(null);
        assertNull(result);
    }
}