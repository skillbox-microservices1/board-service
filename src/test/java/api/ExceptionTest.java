package api;

import com.autum.board.api.error.ErrorCode;
import com.autum.board.api.error.handler.CustomExceptionHandler;
import com.autum.board.business.exception.InternalErrorException;
import com.autum.board.business.provider.ProviderException;
import com.autum.board.business.publication.exception.PublicationNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.web.HttpMediaTypeNotAcceptableException;

import java.util.Locale;

import static com.autum.board.api.error.ErrorCode.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

class ExceptionTest {

    private MessageSource messageSource;
    private CustomExceptionHandler exceptionHandler;

    @BeforeEach
    void setup() {
        messageSource = mock(MessageSource.class);
        exceptionHandler = new CustomExceptionHandler(messageSource);
    }

    @Test
    void testPublicationNotFound() {
        var exception = new PublicationNotFoundException();
        var locale = new Locale("en");
        var msg = "Publication not found";

        when(messageSource.getMessage(PUBLICATION_NOT_FOUND.name(), null, locale)).thenReturn(msg);

        var response = exceptionHandler.publicationNotFound(exception, locale);

        verify(messageSource, times(1)).getMessage(PUBLICATION_NOT_FOUND.name(), null, locale);
        assertEquals(PUBLICATION_NOT_FOUND, response.getErrorCode());
        assertEquals("Publication not found", response.getMessage());
        assertNull(response.getDetails());
    }

    @Test
    void testInternalError() {
        var exception = new Throwable("TEST EXCEPTION MSG");
        var locale = new Locale("en");
        var msg = "Internal server error";

        when(messageSource.getMessage(eq(INTERNAL_ERROR.name()), isNull(), eq(locale))).thenReturn(msg);

        var response = exceptionHandler.internalError(exception, locale);

        verify(messageSource, times(1)).getMessage(INTERNAL_ERROR.name(), null, locale);
        assertEquals(INTERNAL_ERROR, response.getErrorCode());
        assertEquals("Internal server error", response.getMessage());
        assertNull(response.getDetails());
    }

    @Test
    void testInternalErrorException() {
        var exception = new InternalErrorException(new Throwable("TEST EXCEPTION MSG"));
        var locale = new Locale("en");
        var msg = "Internal server error";

        when(messageSource.getMessage(eq(INTERNAL_ERROR.name()), isNull(), eq(locale))).thenReturn(msg);

        var response = exceptionHandler.internalError(exception, locale);

        verify(messageSource, times(1)).getMessage(INTERNAL_ERROR.name(), null, locale);
        assertEquals(INTERNAL_ERROR, response.getErrorCode());
        assertEquals("Internal server error", response.getMessage());
        assertNull(response.getDetails());
    }

    @Test
    void testHandleHttpMediaTypeNotAcceptableException() {
        var exception = new HttpMediaTypeNotAcceptableException("TEST EXCEPTION MSG");
        var locale = new Locale("en");
        var msg = "Invalid media type";

        when(messageSource.getMessage(eq(INVALID_MEDIA_TYPE.name()), isNull(), eq(locale))).thenReturn(msg);

        var response = exceptionHandler.handleHttpMediaTypeNotAcceptableException(exception, locale);

        verify(messageSource, times(1)).getMessage(INVALID_MEDIA_TYPE.name(), null, locale);
        assertEquals(INVALID_MEDIA_TYPE, response.getErrorCode());
        assertEquals(msg, response.getMessage());
        assertNull(response.getDetails());
    }

    @Test
    void errorCode_unexpectedException() {
        var result = ErrorCode.getErrorCode(new Throwable());
        assertEquals(INTERNAL_ERROR, result);
    }

    @Test
    void providerException_unexpectedException() {
        var body = "TEST EXCEPTION MSG";
        var status = HttpStatus.BAD_REQUEST;
        var exception = new ProviderException(status.value(), body);

        var result = exceptionHandler.providerException(exception);

        assertEquals(body, result.getBody());
        assertEquals(status, result.getStatusCode());
    }
}