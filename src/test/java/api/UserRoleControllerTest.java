package api;

import com.autum.board.api.UserRoleController;
import com.autum.board.api.response.PublicationResponse;
import com.autum.board.business.publication.PublicationService;
import com.autum.board.business.publication.PublicationServiceImpl;
import com.autum.board.business.publication.dto.CreatePublicationDto;
import com.autum.board.business.publication.dto.PublicationDto;
import com.autum.board.infrastructure.mapstruct.Mapper;
import com.autum.board.utils.Generator;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.oauth2.jwt.Jwt;


import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;


class UserRoleControllerTest {


    @Test
    void testCreatePublication() {

        var uuid = Generator.generateUUID();

        var title = "Test Publication";
        var description = "Test Description";
        var userUuid = "VKIOPFDCVB67854XDSR789HJUY65ER54";

        var image1 = new MockMultipartFile("testName1.jpg", "image1".getBytes());
        var image2 = new MockMultipartFile("testName2.jpg", "image2".getBytes());
        var images = new MockMultipartFile[]{image1, image2};

        var publicationService = mock(PublicationService.class);
        var mapper = mock(Mapper.class);
        var controller = new UserRoleController(publicationService, mapper);

        //TODO
        var dto = CreatePublicationDto.builder()
                .title(title)
                .description(description)
                .images(List.of(images))
                .build();

        var imageUrl1 = "https://localhost:8978/" + userUuid + "/" + uuid + "/FH34RYDAS4DFJE32432FHE23432IHFEF.jpg";
        var imageUrl2 = "https://localhost:8978/" + userUuid + "/" + uuid + "/FH34RYDAS4DFJE32432FHE23432IHFEF.jpg";
        var imageUrls = List.of(imageUrl1, imageUrl2);

        var publicationDto = PublicationDto.builder()
                .id(1L)
                .uuid(uuid)
                .userUuid(userUuid)
                .title("One")
                .description("First pub")
                .imageUrls(imageUrls)
                .createdAt(System.currentTimeMillis())
                .status(PublicationDto.Status.ACTIVE)
                .build();

        var jwt = getToken();

        when(publicationService.create(any(CreatePublicationDto.class), eq(jwt.getTokenValue())))
                .thenReturn(publicationDto);

        var publicationResponse = new PublicationResponse();
        publicationResponse.setUuid(publicationDto.getUuid());
        publicationResponse.setUserUuid(publicationDto.getUserUuid());
        publicationResponse.setTitle(publicationDto.getTitle());
        publicationResponse.setDescription(publicationDto.getDescription());
        publicationResponse.setCreatedAt(publicationDto.getCreatedAt());
        publicationResponse.setStatus("ACTIVE");
        publicationResponse.setImageUrls(imageUrls);

        when(mapper.map(publicationDto, PublicationResponse.class))
                .thenReturn(publicationResponse);

        var result = controller.create(title, description, images, jwt);

        assertEquals(publicationResponse, result);

        verify(publicationService, times(1)).create(any(CreatePublicationDto.class), eq(jwt.getTokenValue()));
        verify(mapper, times(1)).map(publicationDto, PublicationResponse.class);
    }

    @Test
    void testDeletePublication() {
        var uuid = "VFHTYRIDSE12345VBCJDERD45645FDF2";

        var publicationService = Mockito.mock(PublicationServiceImpl.class);
        var controller = new UserRoleController(publicationService, null);

        var jwt = getToken();

        doNothing().when(publicationService).delete(uuid, jwt.getTokenValue());

        controller.delete(uuid, jwt);

        verify(publicationService, times(1)).delete(uuid, jwt.getTokenValue());
    }

    @Test
    void publicationDto() {
        var uuid = "123";
        var userUuid = "123";
        var title = "title";
        var description = "desc";
        var createAt = System.currentTimeMillis();
        var status = "DELETED";
        var imageUrls = List.of("http://localhost:8080/image1.jpeg");

        var res = new PublicationResponse();
        res.setUuid(uuid);
        res.setUserUuid(userUuid);
        res.setTitle(title);
        res.setDescription(description);
        res.setCreatedAt(createAt);
        res.setStatus(status);
        res.setImageUrls(imageUrls);

        assertEquals(uuid, res.getUuid());
        assertEquals(userUuid, res.getUserUuid());
        assertEquals(title, res.getTitle());
        assertEquals(description, res.getDescription());
        assertEquals(createAt, res.getCreatedAt());
        assertEquals(status, res.getStatus());
        assertEquals(imageUrls, res.getImageUrls());
    }

    private Jwt getToken() {
        var issuedAt = Instant.ofEpochMilli(1712083574L);
        var expiredAt = Instant.ofEpochMilli(1798397174L);

        var token = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJ1WEVZaGw4SklOMVpaM0ZuX2tybG82MF9qUkVQRjlvSnRuOE9CYzZUX1BRIn0.eyJleHAiOjE3OTgzOTcxNzQsImlhdCI6MTcxMjA4MzU3NCwianRpIjoiMjQ1MDczYzktNGZiNS00NzhlLThhODItYjM4NTM5NjgzZDFiIiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo4NTU2L3JlYWxtcy9zb2NpYWwtbmV0d29yay1hcHAiLCJhdWQiOiJhY2NvdW50Iiwic3ViIjoiM2ZhMzQwNDQtNTU4Ni00NTczLTg3OGItY2Q1N2RhNmI4MDY2IiwidHlwIjoiQmVhcmVyIiwiYXpwIjoic29jaWFsLW5ldHdvcmstY2xpZW50Iiwic2Vzc2lvbl9zdGF0ZSI6ImQyNDBlZTkzLWFmYzEtNGFmMC1hNTQ2LTllYjYzOGY1M2MzYyIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOlsiLyoiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwiVVNFUlNfVklFV0VSIiwidW1hX2F1dGhvcml6YXRpb24iLCJkZWZhdWx0LXJvbGVzLXNvY2lhbC1uZXR3b3JrLWFwcCIsIkJPQVJEX1ZJRVdFUiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sInNjb3BlIjoiZW1haWwgcHJvZmlsZSIsInNpZCI6ImQyNDBlZTkzLWFmYzEtNGFmMC1hNTQ2LTllYjYzOGY1M2MzYyIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJuYW1lIjoiSGFuIFNvbG8iLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJ4YW4iLCJnaXZlbl9uYW1lIjoiSGFuIiwiZmFtaWx5X25hbWUiOiJTb2xvIiwiZW1haWwiOiJlbWFpbEBnbWFpbC5jb20ifQ.B5FhyDOhDPgHZOBbfOvMcJrrAui4rJWgGSB1uYDQsOnTXLWn9kBK5_gXEFqQ4jyDc1AX5onfK9N52c-WzjfSIPneyYUZGbjHZR8yL69M0sAJdBJ692ASFgC8wivigtZEivRRyz54lqzwCcb4yw1JN4yU4h1Z116O2OCAm32KpZrQ4Rasivke6o0SANy5k3VJCliHZZ3RF3ZvXPjiLJ5a_KtL-nW_tHg6OPHTbqmlWiKGnBGzp4MmCKTvFqxJZXpbvwYIz2Z4LgJ_t8TR1rJCXcC_0ZMd8y4qT574FI5Z5Q8VMIa733GLVq6khvA501zpLWHg_dhNBXAvwQN-nTueug";
        var headers = new HashMap<String, Object>();
        headers.put("alg", "RS256");
        headers.put("typ", "JWT");
        headers.put("kid", "uXEYhl8JIN1ZZ3Fn_krlo60_jREPF9oJtn8OBc6T_PQ");
        var claims = new HashMap<String, Object>();
        claims.put("exp", "1798397174");
        claims.put("iat", "1798397174");
        claims.put("jti", "245073c9-4fb5-478e-8a82-b38539683d1b");
        claims.put("iss", "http://localhost:8556/realms/social-network-app");
        claims.put("aud", "account");
        claims.put("sub", "3fa34044-5586-4573-878b-cd57da6b8066");
        claims.put("typ", "Bearer");
        claims.put("azp", "social-network-client");
        claims.put("session_state", "d240ee93-afc1-4af0-a546-9eb638f53c3c");
        claims.put("acr", "1");
        claims.put("scope", "email profile");
        claims.put("sid", "d240ee93-afc1-4af0-a546-9eb638f53c3c");
        claims.put("email_verified", "true");
        claims.put("name", "Han Solo");
        claims.put("preferred_username", "xan");
        claims.put("given_name", "Han");
        claims.put("family_name", "Solo");
        claims.put("email", "email@gmail.com");

        var rolesAccount = List.of("manage-account", "manage-account-links", "view-profile");
        var accountRoles = Map.of("roles", rolesAccount);
        var resourceAccess = Map.of("account", accountRoles);
        claims.put("resource_access", resourceAccess);

        var rolesRealm = List.of("offline_access", "USERS_VIEWER", "BOARD_VIEWER", "uma_authorization", "default-roles-social-network-app");
        var realmAccess = Map.of("roles", rolesRealm);
        claims.put("realm_access", realmAccess);

        var allowedOrigin = List.of("/*");
        claims.put("allowed-origins", allowedOrigin);

        return new Jwt(token, issuedAt, expiredAt, headers, claims);
    }
}