package api;

import com.autum.board.api.SharedController;
import com.autum.board.api.response.PublicationResponse;
import com.autum.board.business.publication.PublicationServiceImpl;
import com.autum.board.business.publication.dto.PublicationDto;
import com.autum.board.infrastructure.mapstruct.Mapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;


class SharedControllerTest {

    @Test
    void testGetPublication() {
        var uuid = "VFHTYRIDSE12345VBCJDERD45645FDFD";
        var userUuid = "VFHTYRIDSE12345VBCJDERD45645FDFD";
        var url = "https://image.com/bucket1/";
        var dto = PublicationDto.builder()
                .id(1L)
                .uuid(uuid)
                .userUuid(userUuid)
                .title("Test publication")
                .description("description test publication")
                .createdAt(System.currentTimeMillis())
                .status(PublicationDto.Status.ACTIVE)
                .imageUrls(List.of(url + userUuid + "/image1.jpg", url + userUuid + "/image2.jpg"))
                .build();

        var response = new PublicationResponse();
        response.setUuid(dto.getUuid());
        response.setUserUuid(dto.getUserUuid());
        response.setTitle(dto.getTitle());
        response.setDescription(dto.getDescription());
        response.setCreatedAt(dto.getCreatedAt());
        response.setStatus(dto.getStatus().name());
        response.setImageUrls(dto.getImageUrls());

        var publicationService = Mockito.mock(PublicationServiceImpl.class);
        var mapper = Mockito.mock(Mapper.class);
        var controller = new SharedController(publicationService, mapper);

        when(publicationService.get(uuid)).thenReturn(dto);
        when(mapper.map(dto, PublicationResponse.class)).thenReturn(response);

        var result = controller.get(uuid);

        assertThat(result).isEqualTo(response);
        verify(publicationService, times(1)).get(uuid);
        verify(mapper, times(1)).map(dto, PublicationResponse.class);
    }
}