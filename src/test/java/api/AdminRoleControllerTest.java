package api;

import com.autum.board.api.AdminRoleController;
import com.autum.board.business.publication.PublicationServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.*;

class AdminRoleControllerTest {

    @Test
    void testDeletePublication() {
        var uuid = "VFHTYRIDSE12345VBCJDERD45645FDF2";

        var publicationService = Mockito.mock(PublicationServiceImpl.class);
        var controller = new AdminRoleController(publicationService);

        doNothing().when(publicationService).delete(uuid);

        controller.delete(uuid);

        verify(publicationService, times(1)).delete(uuid);
    }
}