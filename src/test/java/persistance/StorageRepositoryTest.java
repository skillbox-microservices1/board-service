package persistance;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import com.autum.board.persistence.storage.StorageRepositoryImpl;
import com.autum.board.properties.FileStorageProperties;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockMultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class StorageRepositoryTest {

    @Test
    void testFileNameList() throws NoSuchFieldException, IllegalAccessException {
        var prefix = "KFHDYRIDSERTPOID345VBGFH78DD75UI/EIFJEIFDJDEIDJ87972DFESE453CXNBH/";
        var bucket = "test-bucket";

        var key1 = prefix + "file1.txt";
        var key2 = prefix + "file2.txt";

        var properties = mock(FileStorageProperties.class);
        var storage = mock(AmazonS3.class);
        var storageRepository = new StorageRepositoryImpl(storage, properties);

        var objectSummaries = new ArrayList<S3ObjectSummary>();
        var summary1 = new S3ObjectSummary();
        summary1.setKey(key1);
        objectSummaries.add(summary1);
        var summary2 = new S3ObjectSummary();
        summary2.setKey(key2);
        objectSummaries.add(summary2);

        var objectsV2 = new ListObjectsV2Result();
        var field = objectsV2.getClass().getDeclaredField("objectSummaries");
        field.setAccessible(true);
        field.set(objectsV2, objectSummaries);

        when(properties.getBucket()).thenReturn(bucket);
        when(storage.listObjectsV2(bucket, prefix)).thenReturn(objectsV2);

        var expectedResult = Arrays.asList(key1, key2);
        var result = storageRepository.fileNameList(prefix);

        assertEquals(expectedResult, result);
        verify(storage, times(1)).listObjectsV2(bucket, prefix);
        verify(properties, times(1)).getBucket();
    }

    @Test
    void testPut() throws IOException {
        var path = "KFHDYRIDSERTPOID345VBGFH78DD75UI/EIFJEIFDJDEIDJ87972DFESE453CXNBH/file.jpeg";
        var bucket = "test-bucket";

        var image = new MockMultipartFile("images", "image1.jpg", "image/jpg", "dummy image1".getBytes());


        var properties = mock(FileStorageProperties.class);
        var storage = mock(AmazonS3.class);
        var storageRepository = new StorageRepositoryImpl(storage, properties);

        when(properties.getBucket()).thenReturn(bucket);
        var res = new PutObjectResult();
        var obj = new ObjectMetadata();
        obj.setContentLength(image.getSize());
        when(storage.putObject(eq(bucket), eq(path), any(InputStream.class), any(ObjectMetadata.class))).thenReturn(res);

        storageRepository.put(path, image.getInputStream(), image.getSize());

        verify(storage, times(1)).putObject(eq(bucket), eq(path), any(InputStream.class), any(ObjectMetadata.class));
        verify(properties, times(1)).getBucket();
    }

    @Test
    void testDelete() throws IllegalAccessException, NoSuchFieldException {
        var prefix = "KFHDYRIDSERTPOID345VBGFH78DD75UI/EIFJEIFDJDEIDJ87972DFESE453CXNBH/";
        var bucket = "test-bucket";

        var key1 = prefix + "file1.txt";
        var key2 = prefix + "file2.txt";

        var properties = mock(FileStorageProperties.class);
        var storage = mock(AmazonS3.class);
        var storageRepository = new StorageRepositoryImpl(storage, properties);

        var objectSummaries = new ArrayList<S3ObjectSummary>();
        var summary1 = new S3ObjectSummary();
        summary1.setKey(key1);
        objectSummaries.add(summary1);
        var summary2 = new S3ObjectSummary();
        summary2.setKey(key2);
        objectSummaries.add(summary2);

        var keyObj1 = new DeleteObjectsRequest.KeyVersion(key1);
        var keyObj2 = new DeleteObjectsRequest.KeyVersion(key2);
        var keys = List.of(keyObj1, keyObj2);

        var listObjectsV2 = new ListObjectsV2Result();
        var field = listObjectsV2.getClass().getDeclaredField("objectSummaries");
        field.setAccessible(true);
        field.set(listObjectsV2, objectSummaries);

        var request = new DeleteObjectsRequest(bucket);
        request.setKeys(keys);

        when(properties.getBucket()).thenReturn(bucket);
        when(storage.listObjectsV2(bucket, prefix)).thenReturn(listObjectsV2);

        var delObj1 = new DeleteObjectsResult.DeletedObject();
        var delObj2 = new DeleteObjectsResult.DeletedObject();
        var deleteResponse = new DeleteObjectsResult(List.of(delObj1, delObj2));
        when(storage.deleteObjects(request)).thenReturn(deleteResponse);

        storageRepository.delete(prefix);

        verify(storage, times(1)).deleteObjects(any(DeleteObjectsRequest.class));
        verify(properties, times(1)).getBucket();
        verify(storage, times(1)).listObjectsV2(bucket, prefix);
    }
}