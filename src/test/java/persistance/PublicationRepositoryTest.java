package persistance;


import com.autum.board.business.publication.dto.PublicationDto;
import com.autum.board.infrastructure.mapstruct.Mapper;
import com.autum.board.persistence.publication.PublicationRepositoryImpl;
import com.autum.board.persistence.publication.entity.Publication;
import com.autum.board.persistence.publication.repodb.PublicationRepositoryDb;
import com.autum.board.utils.DateUtil;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static com.autum.board.business.publication.dto.PublicationDto.Status.ACTIVE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;


class PublicationRepositoryTest {

    @Test
    void testGet() {
        var uuid = "UIOPFGDERTYDSAZXC78965GH67UIO896";
        var createdAt = System.currentTimeMillis();

        var repositoryDb = mock(PublicationRepositoryDb.class);
        var mapper = mock(Mapper.class);
        var repository = new PublicationRepositoryImpl(repositoryDb, mapper);

        var publication = new Publication();
        publication.setId(1L);
        publication.setUuid(uuid);
        publication.setUserUuid("USERUUID12FGT33413VBCGGDYREI7878");
        publication.setTitle("Test pub");
        publication.setDescription("Test desc");
        publication.setCreatedAt(DateUtil.getLocalDateTimeUTC(createdAt));
        publication.setStatus(1);

        var publicationDto = PublicationDto.builder()
                .id(publication.getId())
                .uuid(publication.getUuid())
                .userUuid(publication.getUserUuid())
                .title(publication.getTitle())
                .description(publication.getDescription())
                .createdAt(createdAt)
                .status(PublicationDto.Status.ACTIVE)
                .build();

        when(repositoryDb.findByUuid(uuid)).thenReturn(publication);
        when(mapper.map(publication, PublicationDto.class)).thenReturn(publicationDto);

        var result = repository.get(uuid);

        assertEquals(publicationDto, result);

        verify(repositoryDb, times(1)).findByUuid(uuid);
        verify(mapper, times(1)).map(publication, PublicationDto.class);
    }

    @Test
    void testGetByUuidAndUserUuid() {
        var uuid = "UIOPFGDERTYDSAZXC78965GH67UIO896";
        var userUuid = "USERUUID12FGT33413VBCGGDYREI7878";
        var createdAt = System.currentTimeMillis();

        var repositoryDb = mock(PublicationRepositoryDb.class);
        var mapper = mock(Mapper.class);
        var repository = new PublicationRepositoryImpl(repositoryDb, mapper);

        var publication = new Publication();
        publication.setId(1L);
        publication.setUuid(uuid);
        publication.setUserUuid("USERUUID12FGT33413VBCGGDYREI7878");
        publication.setTitle("Test pub");
        publication.setDescription("Test desc");
        publication.setCreatedAt(DateUtil.getLocalDateTimeUTC(createdAt));
        publication.setStatus(1);

        var publicationDto = PublicationDto.builder()
                .id(publication.getId())
                .uuid(publication.getUuid())
                .userUuid(publication.getUserUuid())
                .title(publication.getTitle())
                .description(publication.getDescription())
                .createdAt(createdAt)
                .status(PublicationDto.Status.ACTIVE)
                .build();

        when(repositoryDb.findByUuidAndUserUuid(uuid, userUuid)).thenReturn(publication);
        when(mapper.map(publication, PublicationDto.class)).thenReturn(publicationDto);

        var result = repository.getByUuidAndUserUuid(uuid, userUuid);

        assertEquals(publicationDto, result);

        verify(repositoryDb, times(1)).findByUuidAndUserUuid(uuid, userUuid);
        verify(mapper, times(1)).map(publication, PublicationDto.class);
    }

    @Test
    void testSave() {
        var uuid = "UIOPFGDERTYDSAZXC78965GH67UIO896";
        var userUuid = "USERUUID12FGT33413VBCGGDYREI7878";
        var createdAt = System.currentTimeMillis();

        var repositoryDb = mock(PublicationRepositoryDb.class);
        var mapper = mock(Mapper.class);
        var repository = new PublicationRepositoryImpl(repositoryDb, mapper);

        var dto = PublicationDto.builder()
                .uuid(uuid)
                .userUuid(userUuid)
                .title("Test pub")
                .description("Test desc")
                .createdAt(createdAt)
                .status(ACTIVE)
                .build();
        var publication = new Publication();
        publication.setUuid(dto.getUuid());
        publication.setUserUuid(dto.getUserUuid());
        publication.setTitle(dto.getTitle());
        publication.setDescription(dto.getDescription());
        publication.setCreatedAt(DateUtil.getLocalDateTimeUTC(createdAt));
        publication.setStatus(1);


        var savedPublication = new Publication();
        savedPublication.setId(1L);
        savedPublication.setUuid(dto.getUuid());
        savedPublication.setUserUuid(dto.getUserUuid());
        savedPublication.setTitle(dto.getTitle());
        savedPublication.setDescription(dto.getDescription());
        savedPublication.setCreatedAt(DateUtil.getLocalDateTimeUTC(createdAt));
        savedPublication.setStatus(1);

        var savedDto = PublicationDto.builder()
                .id(savedPublication.getId())
                .uuid(savedPublication.getUuid())
                .userUuid(savedPublication.getUserUuid())
                .title(savedPublication.getTitle())
                .description(savedPublication.getDescription())
                .createdAt(createdAt)
                .status(ACTIVE)
                .build();

        when(mapper.map(dto, Publication.class)).thenReturn(publication);
        when(repositoryDb.save(publication)).thenReturn(savedPublication);
        when(mapper.map(savedPublication, PublicationDto.class)).thenReturn(savedDto);

        var result = repository.save(dto);

        assertEquals(savedDto, result);

        verify(repositoryDb, times(1)).save(publication);
        verify(mapper, times(1)).map(dto, Publication.class);
        verify(mapper, times(1)).map(savedPublication, PublicationDto.class);
    }

    @Test
    void publication() {

        var id = 1L;
        var uuid = "UIOPFGDERTYDSAZXC78965GH67UIO896";
        var userUuid = "USERUUID12FGT33413VBCGGDYREI7878";
        var title = "title";
        var description = "desc";
        var time = LocalDateTime.now();
        var status = 3;

        var publication = new Publication();
        publication.setId(id);
        publication.setUuid(uuid);
        publication.setUserUuid(userUuid);
        publication.setTitle(title);
        publication.setDescription(description);
        publication.setCreatedAt(time);
        publication.setStatus(status);
        publication.setDeletedAt(time);

        assertEquals(id, publication.getId());
        assertEquals(uuid, publication.getUuid());
        assertEquals(userUuid, publication.getUserUuid());
        assertEquals(title, publication.getTitle());
        assertEquals(description, publication.getDescription());
        assertEquals(time, publication.getCreatedAt());
        assertEquals(time, publication.getDeletedAt());
        assertEquals(status, publication.getStatus());
    }
}