package config;

import com.autum.board.config.FileStorageConfig;
import com.autum.board.config.SwaggerConfig;
import com.autum.board.config.WebConfig;
import com.autum.board.properties.FileStorageProperties;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

//TODO у конфига проверки только на null
class ConfigTest {

    @Test
    void storage() {
        var config = new FileStorageConfig();
        var properties = new FileStorageProperties();
        properties.setEndpoint("http://localhost:9000/");
        properties.setExternalHost("http://localhost:9000/");
        properties.setSinger("AWSS3V4SignerType");
        properties.setAccessKey("DBHWYWcE9NuNlTqp03B0");
        properties.setSecretKey("4OODI6oLBTguo8BTMe9yIThmuehtN0TI7o6o0C1Y");
        properties.setRegion("rus");
        properties.setBucket("board-images");

        Assertions.assertEquals("http://localhost:9000/", properties.getExternalHost());
        Assertions.assertEquals("board-images", properties.getBucket());

        var result = config.storage(properties);
        Assertions.assertNotNull(result);
    }

    @Test
    void messageSource() {
        var config = new WebConfig();
        var result = config.messageSource();
        Assertions.assertNotNull(result);
    }

    @Test
    void localeResolver() {
        var config = new WebConfig();
        var result = config.localeResolver();
        Assertions.assertNotNull(result);
    }

    @Test
    void logFilter() {
        var config = new WebConfig();
        var result = config.logFilter();
        Assertions.assertNotNull(result);
    }

    @Test
    void swaggerApi() {
        var config = new SwaggerConfig();
        var result = config.api();
        Assertions.assertNotNull(result);
    }

}
