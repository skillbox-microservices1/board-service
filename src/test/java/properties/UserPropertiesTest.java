package properties;

import com.autum.board.properties.UserProperties;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class UserPropertiesTest {

    @Test
    void test() {

        var url = "http://autum.com";

        var properties = new UserProperties();
        properties.setUrl(url);

        assertEquals(url, properties.getUrl());
    }
}