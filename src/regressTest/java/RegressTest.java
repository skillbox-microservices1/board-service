import com.amazonaws.services.s3.AmazonS3;
import com.autum.board.BoardApplication;
import com.autum.board.api.response.PublicationResponse;
import com.autum.board.business.provider.user.UserDto;
import com.autum.board.properties.FileStorageProperties;
import com.google.gson.Gson;
import config.TestConfig;
import org.junit.jupiter.api.Test;
import org.mockserver.client.MockServerClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.util.LinkedMultiValueMap;

import javax.annotation.PostConstruct;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;


@SpringBootTest(classes = BoardApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = TestConfig.class)
public class RegressTest extends AbstractTest {

    @LocalServerPort
    private int port;

    private final MockServerClient mockServerClient = new MockServerClient(MOCK_HTTP_SERVER.getHost(), MOCK_HTTP_SERVER.getServerPort());

    @Autowired
    private TestRestTemplate client;
    @Autowired
    private AmazonS3 storage;
    @Autowired
    private FileStorageProperties storageProperties;


    @PostConstruct
    public void init() {
        if (!storage.doesBucketExistV2(storageProperties.getBucket())) {
            storage.createBucket(storageProperties.getBucket());
        }
    }

    /**
     * Создание публикации пользователм
     * Получение публикации
     * Удаление публикации
     */
    @Test
    public void userScenario() throws IOException {

        var baseUrl = TRANSFER_PROTOCOL.toLowerCase() + "://localhost:" + port + "/api/v1/publications";

        var username = "autum";
        var image = new ClassPathResource("java.jpg");

        var accessToken = getAccessToken(username, client);
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        var auth = "Bearer " + accessToken;
        headers.set("Authorization", "Bearer " + accessToken);

        LinkedMultiValueMap<String, Object> multipartRequestParts = new LinkedMultiValueMap<>();
        multipartRequestParts.add("title", "Main post");
        multipartRequestParts.add("description", "first post");
        multipartRequestParts.add("images", new FileSystemResource(image.getFile()));

        var userDto = new UserDto();
        userDto.setUuid("RUFDCV7890FDERE6709DEWDE324VBFDF");
        userDto.setFirstName("Nick");
        userDto.setLastName("One");
        userDto.setMiddleName("Wan");

        var city = new UserDto.CityDto();
        city.setUuid("11FDCV7890FDERE6709DEWDE324VB321");
        city.setName("Asgard");
        city.setCountry("Asgard");
        city.setRegion("Valhalla");

        userDto.setCity(city);
        userDto.setStatus(UserDto.Status.ACTIVE);
        userDto.setSex(UserDto.Sex.MALE);
        userDto.setSubscriptions(100L);
        userDto.setSubscribers(100L);

        mockServerClient
                .when(request()
                        .withMethod("GET")
                        .withPath("/api/v1/users/yourself")
                        .withHeader("Authorization", auth)
                )
                .respond(response()
                        .withBody(new Gson().toJson(userDto))
                );

        var response = client.exchange(
                baseUrl + "/create",
                HttpMethod.POST,
                new HttpEntity<>(multipartRequestParts, headers),
                PublicationResponse.class
        );

        //TODO проверка тела response

        assertEquals(HttpStatus.OK, response.getStatusCode());
        var body = response.getBody();
        assertNotNull(body);
        //TODO проверка базы, что публикация создана

        //Получаем публикацию
        var url = baseUrl + "/" + body.getUuid();

        var responsePublication = client.exchange(url, HttpMethod.GET, null, PublicationResponse.class);
        //TODO проверка body
        assertEquals(HttpStatus.OK, responsePublication.getStatusCode());

        //удаляем публикацию
        headers.setContentType(null);
        var httpEntity = new HttpEntity<>(null, headers);
        var responseDelete = client.exchange(url, HttpMethod.DELETE, httpEntity, Void.class);
        assertEquals(HttpStatus.OK, responseDelete.getStatusCode());
        //TODO проверка базы, что публикация удалена

        //Пытаемся получить удаленную публикацию
        var responseDeletedPublication = client.exchange(url, HttpMethod.GET, null, PublicationResponse.class);
        //TODO проверка body с ошибкой
        assertEquals(HttpStatus.NOT_FOUND, responseDeletedPublication.getStatusCode());
    }
}