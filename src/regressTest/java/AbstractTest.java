import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.mockserver.client.MockServerClient;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.util.LinkedMultiValueMap;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.MockServerContainer;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.utility.DockerImageName;
import org.testcontainers.utility.MountableFile;


public abstract class AbstractTest {

    protected static final String TRANSFER_PROTOCOL = "HTTPS";

    protected static final Integer KEYCLOAK_PORT = 8443;
    protected static final Integer KEYCLOAK_PORT_TWO = 8080;
    private static final int MINIO_CONSOLE_PORT = 9001;
    private static final int MINIO_PORT = 9000;
    private static final String MINIO_ACCESS_KEY = "board-service";
    private static final String MINIO_SECRET_KEY = "F42IUUEZXCqwrweri23324FFASMN";
    private static final String BUCKET_NAME = "board-service";

    protected static final MockServerContainer MOCK_HTTP_SERVER = new MockServerContainer(DockerImageName
            .parse("mockserver/mockserver")
            .withTag("mockserver-" + MockServerClient.class.getPackage().getImplementationVersion()));


    protected static final PostgreSQLContainer POSTGRES_CONTAINER = new PostgreSQLContainer<>(
            DockerImageName.parse("postgres:16.0")
    )
            .withDatabaseName("db")
            .withUsername("postgres")
            .withPassword("123")
            .withCommand("-c ssl=on -c ssl_cert_file=/etc/ssl/certs/ssl-cert-snakeoil.pem -c ssl_key_file=/etc/ssl/private/ssl-cert-snakeoil.key");

    protected static final GenericContainer KEYCLOAK_CONTAINER = new GenericContainer<>("quay.io/keycloak/keycloak:23.0.7")
            .withExposedPorts(KEYCLOAK_PORT)
            .withCommand(
                    "-Dkeycloak.migration.action=import",
                    "-Dkeycloak.migration.provider=dir",
                    "-Dkeycloak.migration.dir=/opt/keycloak",
                    "start-dev"
            )
            .withEnv("KEYCLOAK_ADMIN", "admin")
            .withEnv("KEYCLOAK_ADMIN_PASSWORD", "admin")
            .withEnv("KC_HTTPS_CERTIFICATE_FILE", "/opt/keycloak/keycloak-crt.pem")
            .withEnv("KC_HTTPS_CERTIFICATE_KEY_FILE", "/opt/keycloak/keycloak-key.pem")
            .withCopyFileToContainer(MountableFile.forClasspathResource("keycloak-social-network-realm.json"), "/opt/keycloak/keycloak-social-network-realm.json")
            .withCopyFileToContainer(MountableFile.forClasspathResource("keycloak_cert.pem"), "/opt/keycloak/keycloak-crt.pem")
            .withCopyFileToContainer(MountableFile.forClasspathResource("keycloak_key.pem"), "/opt/keycloak/keycloak-key.pem");

    public static final GenericContainer MINIO_CONTAINER = new GenericContainer<>("minio/minio:RELEASE.2024-04-06T05-26-02Z")
            .withExposedPorts(MINIO_CONSOLE_PORT, MINIO_PORT)
            .withAccessToHost(true)
            .withEnv("MINIO_ROOT_USER", MINIO_ACCESS_KEY)
            .withEnv("MINIO_ROOT_PASSWORD", MINIO_SECRET_KEY)
            .withCommand("server --console-address :" + MINIO_CONSOLE_PORT + " /data")
            .withCopyFileToContainer(MountableFile.forClasspathResource("minio.key"), "/root/.minio/certs/private.key")
            .withCopyFileToContainer(MountableFile.forClasspathResource("minio.crt"), "/root/.minio/certs/public.crt");

    @BeforeAll
    public static void setUp() {
        try {
            POSTGRES_CONTAINER.start();
            KEYCLOAK_CONTAINER.start();
            MINIO_CONTAINER.start();
            MOCK_HTTP_SERVER.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @DynamicPropertySource
    protected static void overrideProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", POSTGRES_CONTAINER::getJdbcUrl);
        registry.add("spring.datasource.username", POSTGRES_CONTAINER::getUsername);
        registry.add("spring.datasource.password", POSTGRES_CONTAINER::getPassword);
        registry.add("spring.datasource.driver-class-name", POSTGRES_CONTAINER::getDriverClassName);

        registry.add("spring.security.oauth2.resourceserver.jwt.issuer-uri", () -> getKeycloakHost() + "/realms/social-network-app");

        int port = MINIO_CONTAINER.getMappedPort(MINIO_PORT);
        registry.add("aws-storage.endpoint", () -> TRANSFER_PROTOCOL.toLowerCase() + "://" + MINIO_CONTAINER.getHost() + ":" + port + "/");
        registry.add("aws-storage.external-host", () -> TRANSFER_PROTOCOL.toLowerCase() + "://localhost:9000/");
        registry.add("aws-storage.singer", () -> "AWSS3V4SignerType");
        registry.add("aws-storage.access-key", () -> MINIO_ACCESS_KEY);
        registry.add("aws-storage.secret-key", () -> MINIO_SECRET_KEY);
        registry.add("aws-storage.region", () -> "rus");
        registry.add("aws-storage.bucket", () -> BUCKET_NAME);
        registry.add("aws-storage.protocol", () -> TRANSFER_PROTOCOL);

        registry.add("user-config.url", () -> "http://" + MOCK_HTTP_SERVER.getHost() + ":" + MOCK_HTTP_SERVER.getServerPort());
    }

    protected static String getKeycloakHost() {
        int kcPort = KEYCLOAK_CONTAINER.getMappedPort(KEYCLOAK_PORT);
        return TRANSFER_PROTOCOL.toLowerCase() + "://" + KEYCLOAK_CONTAINER.getHost() + ":" + kcPort;
    }

    protected String getAccessToken(String username, TestRestTemplate client) {
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        var map = new LinkedMultiValueMap<>();
        map.add("username", username);
        map.add("password", "123");
        map.add("grant_type", "password");
        map.add("client_id", "social-network-client");

        var request = new HttpEntity<>(map, headers);
        var host = getKeycloakHost();

        var response = client.postForEntity(host + "/realms/social-network-app/protocol/openid-connect/token", request, String.class);

        if (response.getStatusCode() == HttpStatus.OK) {
            try {
                var jsonObject = new JSONObject(response.getBody());
                return jsonObject.getString("access_token");
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        } else {
            throw new RuntimeException("Failed to get access token. Status code: " + response.getStatusCodeValue());
        }
    }
}
