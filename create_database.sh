if ! psql -U postgres -lqt | cut -d \| -f 1 | grep -qw "Board" ; then
    psql -U postgres -c "CREATE DATABASE Board;"
fi