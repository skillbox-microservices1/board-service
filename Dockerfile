FROM fredrikhgrelland/alpine-jdk11-openssl
WORKDIR /app
COPY build/libs/*.jar ./board.jar

CMD ["java", "-jar", "board.jar"]